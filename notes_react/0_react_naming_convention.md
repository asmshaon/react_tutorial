
# Types of naming convention


1. Camel -> e.g: AppContainer.js
	- both file and component name

2. Pascel -> e.g: AppContainer.js
	- both file and component name

3. Kebab -> e.g: app-container.js
	- used for file names only.
	- rearely
