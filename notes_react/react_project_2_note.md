
# Topic: Adding Routing in New React Project

## Installing the necessary packages

___code:___
```js
npm install react-router-dom
```

* Introduce the router into the project:


file: {project_directory}/src/index.js


___code:___
```js
import { BrowserRouter } from "react-router-dom";

```

## Navigation code block

* Problem with using hyperlink
	- aka <a link="https://www.google.com"></a>
	- Disagvantage: Whenever the link is clicked a new request will be sent to the server. Then the server will reply with our application and then the router will figure out which page to load.
	- But react is a single page app. As such we already are in our running react application. So activation of a link creates lots of redundent process. We don't need to leave our current application just because we want to navigate somewhere.

* Solution:

	- Instead of using an anchor tag we use another component provided by 'react-router-dom'
	- __Component:__ 'Link'.
	- React-router-dom attaches a clickListener to __Link__ component.
	- With this react bypasses all the problem caused by using an anchor tag.
	- No more sending extra unnecessary requests.

___Code:___

file: {project_directory}/src/component/layout/MainNavigation.js

```jsx

import { Link } from 'react-router-dom';

function MainNavigation() {
  return (
    <header>
      <div>React Meetups</div>
      <nav>
        <ul>
          <li>
            <Link to="/">Home Page</Link>
          </li>
          <li>
            <Link to="/new">New Meetup Page</Link>
          </li>
          <li>
            <Link to="/favorite">Favorite Page</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

}

export default MainNavigation;
```

## Component specific CSS

	- React provides components to make a CSS file figure out the scope necessary for a certain components.

* Using this:

	- First of all, the name of the css file needs to follow a certin standard of naming.
	- css files must have ___.module.css___ extention, e.g. cssFile.module.css


## JSON

* fetch by default sends a get request.
* fetch returns a promise.

fetch(
	"URL",
	{
		method: "POST",
		body: JSON.stringify(meetupData),
		headers: {
			'Content-Type': 'application/json'
		}
	}
);

## useEffect

```js
useeffect(() => {
	fetch(
	"URL", {}
	).then()
}, []);
)
})
```
