
# Topic: React tutorial notes

* Note\_Number: 1
* Source: youtube video; "React Crash course for beginners 2021"

## React

* without the help of a framework to build an UI, we have to write the codes for each elements an a descriptive pattern. With the end result being a lot of similar looking lines being repeated quite a lot. This programming style is called "___Imperative Approach___".

* This is a relatively lean framework. As in, it was designed to be only used as a tool to buld UI components with. React doesn't  ship with any other features .e.g. routing. Heigherarchy of Frontend framework of js is given below. These are listed based on feature richness in descending order.

	1. Angular.js - Feature rich - Heavy
	2. vue.js - Less rich than Angular but reacher than react - Moderate
	3. React.js - least feature rich - Lean

* Single page Development

* Needs a development server. Needs to have proper environment to set up the project, transform the codes, and finally run the project. All of this is achieved through an external tool, called "___create-react-app___".

	* "create-react-app" tool dependency:
		- node.js needs to be installed in the system. It is a JavaScript runtime



## Start of Project

### 1. Create react Project

#### 1.1 Create a default project with terminal command

important: Needs internet connection


> tools: create-react-app
>
> location: ~/codes/react_tutorial/

___code:___

```bash
npx create-react-app {project-name}
```

This creates a dummy project in the newly created project directory. The directory is named as the given project-name.

* To start the project:


___code:___

```bash
cd {project-name}
npm start
```

__N.B:__ This should start the project automatically in the browser. If that doesn't happen, we can open a new tab the browser and go to:
"http://localhost:3000/" to see the execution.


#### 1.2 Project Structure

**Default project structure**

```bash
.
node_modules/
package.json
package-lock.json
README.md
Public/
src
	App.css
	App.js
	App.test.js
	index.css
	index.js
	logo.svg
	reportWebVitals.js
	setupTests.js

```


__Needed project structure:__

```bash

node_modules/
package.json
package-lock.json
README.md
Public/
src
   App.js
   index.css
   index.js

```

* Command to get the structure:

```bash
cd {project-structure}
trash App.css App.test.js logo.svg reportWebVitals.js setupTests.js
```

#### Adding Directories

* According to standard practise all components needs to reside in a single deirectory called __"components"__.
* Relative location: _{project_directory}/src/components_.

```bash

node_modules/
package.json
package-lock.json
README.md
Public/
src
   components/   <----------- New Dir
   App.js
   index.css
   index.js

```


# RAW
* Special HTML format for react -> jsx
* Can not use 'class' in jsx. Because 'class' is a keyword in js.
In JS HTML property called 'class' is replaced with __'className'__

___code:___

```js
function App(){
	return (
		<div className='card'>
		<h1>Card HTML</h1>
		<div className='btn'>
			<button>Delete Button</button>
		<div>
		</div>
	);
}

````
* Key Words:
	- Single Page Application
	- Overlay(Modal??)
	- Imperative approach
	- Declarative approach
	- React Hook ( e.g: useState )
	- JS: array destructuring

#### React Component:
* A react component is just a function.
* Standard practise in naming a components is to capitalize the first letter. React wants custom components to start with a capital charecter.

___In Todo.js:___

```js
function Todo() {
	return();
}

export default Todo;

```

* Props: for building reuse able components
* state: important for controlling what we see on a screen

#### Using State:

* State is an very important concept of react; used to control what is shown on a screen.
* calling state:

___code:___

```js
import {useState} from 'react';

const [ value, setValueFunc ] = useState(false)

function valueTrueHandler(){
	setValueFunc(true)
}

function valueFalseHandler(){
	setValueFunc(false)
}

```

- useState always returns an array with 2 elements
- First: Given / Default value; Second: function that toggles the First value




