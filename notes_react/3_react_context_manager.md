
# Context Manager in React

## Creating Context

* import built in React component.
```js
	import { createContext} from 'react';
```

* Create the Context Manager instance
	- Convention is to name CAPITALIZE the name of the context manager.

```js
	const FavoritesContext = createContext();
```
