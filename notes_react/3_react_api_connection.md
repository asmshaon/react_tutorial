
# Topic: API Connection to a React front-end

## Start: Basic

* A react frontend should never be connected to a database directly for security reasons.
	- A react based application exposes the code to the user via "__Inspect__".
	- As such if it is connected to a database, the database credentialas would be exposed to the public.

* Connect the react app to an API. That API will be in change of communicating to the database.


# Tutorial Notres: Start

##  Backend tech:
	- Firebase

## Creating a virtual database:

* One of the functions provided by firebase is creation of:

	1. RealTime Database
	2. An API

### Create a Realtime Database:

* Steps:

```text
Create Project -> Agreed to User Analytics this time -> Realtime database -> Test Mode
```

* In fetch Request:

* We can add parts after the base URL. The added part will be considered as a table.
* __NOTE:__ Firebase expects ".json" extention to be included after the additional part.


## Send HTTP request:

* function: fetch()

>> Built in browser(JS?) function

## useEffect:

* library: react
*





