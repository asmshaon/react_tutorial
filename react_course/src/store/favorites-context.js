import { getValue, useState } from "@testing-library/user-event/dist/utils";
import { createContext } from "react";

const FavoritesContext = createContext({
  favorites: [],
  totalFavorites: 0,
});

function FavoritesContextProvider(props) {
  // To update the context getValue
  const { userFavorites, setUserFavorites } = useState([]);

  function addFavoritesHandler() {}

  function removeFavoritesHandler() {}

  function itemIsFavoritesHandler() {}

  const context = {
    favorites: userFavorites,
    totalFavorites: userFavorites.length,
  };

  return (
    <FavoritesContext.Provider value={context}>
      {props.children}
    </FavoritesContext.Provider>
  );
}
