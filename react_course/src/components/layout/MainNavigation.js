import { Link } from "react-router-dom";
import classes from "./MainNavigation.module.css";

function MainNviation() {
  return (
    <header className={classes.header}>
      <div className={classes.logo}>React Meetups</div>
      <nav>
        <ul>
          <li>
            <Link to="/">Home Page</Link>
          </li>
          <li>
            <Link to="/new">New Meetup Page</Link>
          </li>
          <li>
            <Link to="/favorite">Favorite Page</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default MainNviation;
