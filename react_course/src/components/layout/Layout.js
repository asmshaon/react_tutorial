import classes from "./Layout.module.css";
import MainNviation from "./MainNavigation";

function Layout(props) {
  return (
    <div>
      <MainNviation />
      <main className={classes.main}>{props.children}</main>
    </div>
  );
}

export default Layout;
