function Modal(props) {
  return (
    <div className="modal">
      <p>Are You sure?</p>
      <button className="btn btn--alt" onClick={props.onCancle}>
        Cancel
      </button>
      <button className="btn" onClick={props.onConfirm}>
        Confirm
      </button>
    </div>
  );
}

export default Modal;
