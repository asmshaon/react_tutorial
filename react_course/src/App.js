import { Routes, Route } from "react-router-dom";

import AllMeetupsPages from "./pages/AllMeetups";
import NewMeetupsPages from "./pages/NewMeetups";
import FavoritesPage from "./pages/Favorites";
import Layout from "./components/layout/Layout";

function App() {
  // localhost:
  return (
    <Layout>
      <Routes>
        <Route index element={<AllMeetupsPages />} />
        <Route path="/new" element={<NewMeetupsPages />} />
        <Route path="/favorite" element={<FavoritesPage />} />
      </Routes>
    </Layout>
  );
}

export default App;
